console.log("\n========== MEDIUM HIGHLIGHTS SCRAPER ==========\n");

const jsdom = require("jsdom");
const {JSDOM} = jsdom;
var request = require("request");
var jsonq=require("jsonq");

var name = process.argv[2];
var numPosts = process.argv[3];

profileURL = "https://medium.com/@" + name;

JSDOM.fromURL(profileURL).then(dom => {
    const window = dom.window;

    var $ = require("jquery")(window);

    var userID = $('.followState').attr('data-user-id');

    //console.log(userID);

    var highlightsURL = "https://medium.com/_/api/users/" + userID + "/profile/stream?limit=" + numPosts + "&to=0&source=quotes&pages=1";

    request(highlightsURL, function (error, response, body) {

        var newBody = "";
        // Trim out random garbage characters in the beginning of the body (non-JSON)
        for (var i=16; i<body.length; i++) {
            newBody += body[i];
        }
        
        // Convert to a JSON object for using jsonQ functions
        var object = jsonq(newBody);

        // Find all quoteIDs
        var quoteID = object.find('payload').find('references').find('quoteId').value();

        console.log(numPosts + " most recent highlights by @" + name);

        for (var i = 0; i < quoteID.length; i++) {
            var postID = object.find('payload').find('references').find('Quote').find(quoteID[i]).find('postId').value();
            var postName = object.find('payload').find('references').find('Post').find(postID).find('title').value();
            var postAuthorID = object.find('payload').find('references').find('Post').find(postID).find('creatorId').value();
            var postAuthor = object.find('payload').find('references').find('User').find(postAuthorID).find('name').value();
            var quoteParagraphRaw = object.find('payload').find('references').find('Quote').find(quoteID[i]).find('paragraphs').find('text').value();
            var startOffset = object.find('payload').find('references').find('Quote').find(quoteID[i]).find('startOffset').value();
            var endOffset = object.find('payload').find('references').find('Quote').find(quoteID[i]).find('endOffset').value();
            
            // Convert the array to a string
            var quoteParagraphString = quoteParagraphRaw.join("");
            // Get only the highlighted section
            var quote = quoteParagraphString.substring(startOffset, endOffset)

            // Get a little bit of content before and after the quote

            var paragraphStart = startOffset-60, paragraphEnd = endOffset+60;

            if (paragraphStart < 0) paragraphStart = 0;
            if (paragraphEnd > quoteParagraphString.length) paragraphEnd = quoteParagraphString.length;

            var quoteParagraph = "";
            if (paragraphStart != 0) quoteParagraph += "...";
            quoteParagraph += quoteParagraphString.substring(paragraphStart, paragraphEnd);
            if (endOffset != paragraphEnd) quoteParagraph += "..";



            quoteNumber = parseInt(i);
            quoteNumber++;
            // Output
            outputString = "\nQuote #" + quoteNumber + ": From \"" + postName + "\" by \"" + postAuthor + "\"\n\n" + quoteParagraph + "\n";

            console.log(outputString);
        }
    });

});